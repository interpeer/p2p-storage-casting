\subsection{Authentication, Authorization and Accounting}

The previous section refers to the use of X3DH for exchange of file encryption
keys. This already assumes that some authentication is to be performed via a
public key infrastructure.

\subsubsection{Authentication}

The two major concerns for a PKI in the context of this document are to provide
a chain of authentication, and allow for key revocations. The exact workings of
a PKI are beyond the scope of this document.

The proposed hierarchy of keys as follows:

\begin{enumerate}
  \item Each participant in the network must create a root or master key. This
    key is to be validated or revoked by the PKI, and forms the basis of all
    other operations. We'll call this the {\em identity key}.

  \item Each participant must create one, and may create many device keys.
    These keys allow a participant with multiple devices to join the network.
    Device keys or hashes of them act as peer IDs (discussed later).

    This distinction between identities and devices allows for addressing each
    with distinct data.

    Device keys are signed by identity keys.

  \item Finally, X3DH mentiones ephemeral session keys. Unlike X3DH, such keys
    can be issued by identities {\em or} devices, so the issuer must always be
    referred to in their usage.

    This allows for key rotation across all communication channels.

    Ephemeral keys are signed by identity or device keys, depending on their
    purpose.
\end{enumerate}

From the point of view of the peer-to-peer network, this distinction between
different key types is sufficient for all proposed use-cases.

\subsubsection{Authorization}

Authorization on the network occurs on several levels, but can be separated
into distinct categories.

\paragraph{Network authorization}
refers to peers verifying that identities or their devices may connect to the network.
If an identity is part of the PKI and not revoked, network authorization is
granted.

\paragraph{Service authorization}
refers to peers verifying that a device may perform the requested action --
this is the information that allows peers to deny service.

It is beyond the scope of this document to define all possible types of service
authorization, but later sections of the document will refer to some of them.

At its most basic, service authorization can be granted if a tuple of an
identity and a permission specification is signed by an authority, and no revocation
for this tuple exists.

Any identity can be an authority in this scheme; however, it can only be an
authority over its own permission specifications. That is, permission
specifications are always scoped to the signing authority. Participants in the
network can decide whether or not they accept an authority.

The first step in service authorization is for a participant claiming authority
over some permission specification by publishing this claim, providing the
permission specification and their signature. It may be sufficient to publish
this claim, or the claim needs to be validated.

The exact validation process is outside of the scope of this document. However
it is implemented, the permission specification will include in some form
{\em what action} is permitted on {\em which object}\footnote{Objects could be
groups of other objects; the scoping is very flexible.}.

If a claim is valid, then a signature by the claimant over the permission
specification and an identity becomes a valid permission grant that any node
can independently verify.

Permission revocations are similar signed tuples of the permission
specification, the identity, and a marker indicating a revocation. Timestamping
the tuples for grants and revocations allows for retracing their history.

Subsequent sections of this document assume that {\em service authorization} is
implemented in this fashion.

\paragraph{File authorization}
refers to file access permissions. If the X3DH scheme is used for embedding
encryption keys into the file stream as described previously, then being able
to decrypt such a key record translates to read permissions on the file.

Write permissions are also fairly easily handled, allowing for a file to be
appended to by multiple sources. It makes use of the above service
authorization scheme as follows:

\begin{enumerate}
  \item As a file owner, a participant publishes a claim that they can grant
    write permissions on the file in question. This claim is validated.

  \item The file owner then publishes a permission grant for writing on the
    file. The identity of the grantee is, of course, included.

  \item The permission grantee publishes a file append record (or some such).

  \item Recipients of the record can verify that they can trust the contents,
    because the owner delegated write permissions to the publisher of the
    record.
\end{enumerate}

We should similarly publish read permissions in order for peers to deny service
early. This prevents peers who have lost their cached copies of the file from
re-downloading them.

The proposed encryption scheme using X3DH is still lacking in information on
how prekey bundles are to be distributed.

In the X3DH specification, it is the server's responsibility to hand out one-
time prekeys one by one, until none are left. In a distributed environment,
such accounting is fairly hard to do in a timely fashion, especially when nodes
fail. The proposal, therefore, is to not use one-time prekeys \footnote{An
alternative might be to use short-lived keys, but that introduces the problem
of time synchronization across the network}.

\paragraph{Scoped File Authorization}
is also possible. In the above scheme, we assume that access to a file is
either granted or not.

We can scope permission fairly easily by changing the encryption key before all
file append chunks are added to the Merkle tree. This can be done fairly easily
by employing the Double Ratchet \cite{doubleratchet} algorithm, and publishing
newly generated keys in the same way as described in the previous section as
part of the Merkle tree.

This is not the cheapest operation. All changes to the encryption key need to
be sent to all permitted readers. That may mean encrypting a (small) key
thousands of times, depending on how widely the file is used, and embedding
records for each into the Merkle tree.

The change would require the publishing message of the key to be extended to
refer to the last hash encrypted with the previous key, so that reasonably
rapid random access can be granted.

In the unmodified Double Ratchet scheme, each message (aka chunk) should be
encrypted with a different key. As an optimization, it is proposed that this
key rotation is not performed for every chunk, but rather every time the reader
set changes, and optionally at a configurable interval.

Similarly, write permission grants must be scoped not only to a file, but to a
hash offset -- this is entirely possible under the proposed scheme\footnote{Complications
arise when write permissions are granted and revoked for conflicting offsets.
This must be addressed in an implementation.}.

\paragraph{Delegation}

One of the desirable side effects of the proposed permission scheme is that
it's possible for file owners to delegate jobs to other peers. When casting
video, for example, it is unlikely that a peer has the bandwidth for all
possible video resolutions (and therefore bitrates) that clients require.

A published transcode permission allows its grantee to authoritatively publish
a downscaled video, without the need for the stream producer to sign each
downscaled video chunk.

Similar reasoning applies to peers verifying casting sources other than the
originating peer, if that is a desired feature.

\subsubsection{Accounting}

In the previous sections, the publication of prekey bundles or of permission
claims already require some form of accounting of some actions. Whatever method
chosen for publication, it can serve for accounting for other actions.
