\subsection{Internet Connections}
We routinely build data centers these days with leaf-and-trunk architectures
that see a Gbit/s of bandwidth provisioned for each CPU core, leading to
Tbit/s availability on the trunk, so that it can be sobering to compare
this to standard, affordable internet connection speeds around the world.

\subsubsection{Average Bandwidths}
Cable.co.uk \cite{cable:2019} provides a good overview of world broadband speeds of 2018, which
demonstrates the range to be between 5-60Mbit/s. The 2018 data highlights that
the bottom end of the range improved by maybe half an Mbit/s, whereas the top
improved by ca. 4Mbit/s -- we consider that too little to update the
calculations in further sections.

The average improvement of ca. 25 percent makes a big enough difference for
updates since the last draft, but does not affect the general problem scenario
enough to propose new solutions.

\subsubsection{Minimum Bandwidths}
It’s worth noting that the data released only lists average speeds, no median
or standard deviation. It therefore is unclear how this translates to
geographic distribution; it is a well established pattern that metropolitan
regions enjoy much faster bandwidths than the country side.

There’s a clue in a broadband map Germany publishes \cite{broadbandatlas}. It maps where availability
of broadband speeds exceeds particular percentages. In metropolitan regions,
available broadband speeds in excess of 50Mbit/s are typical, while only speeds
between 6-16Mbit/s seem to be available universally.

Of course, this map lists advertised broadband speeds while Cable's results
list measured speeds. According to the FCC's 80/80 metric of measured speeds
\cite{measured}, median speeds vary greatly from advertised speeds from ISP to
ISP. An average 80/80 performance across ISPs looks to be in the ballpark of
70\% of the advertised speed.

If we apply this assumption to Germany's broadband map, it leaves the conclusion
that universally available, actual broadband speeds in that country should be expected
in the range of 4-11 Mbit/s, with an average of 7.5Mbit/s.

We can compare this average to the 24Mbit/s published for Germany by Cable;
then it is lower by a factor of 3.2. This factor probably does not apply globally,
but without more research, we may assume it does. This leads to a minimum
available bandwidth assumption of 1.5-18.75Mbit/s globally. Applying the
average increase of bandwidth over the last twelve months leads to a new
bandwidth assumption of 1.9-23.5Mbit/s globally.

\subsubsection{Latency}
There is not much published data on internet latency around the world.
Dotcom-Monitor publishes latencies between data centres \cite{dotcomlatency},
which is a useful starting point, but will bear little resemblance to end-user
latency experiences.

The table does offer an insight, however. With inter-city latencies ranging
from the 20s of milliseconds to the 400s, we do know that latency differences
range by about a factor of 20 -- more than an order magnitude. We can safely
assume that latencies for end-users will generally be higher, but follow a
similar order-of-magnitude distribution between various locations on the planet.

There is a second conclusion the table permits, and that is that --
unsurprisingly -- low latency is generally associated with geographic
closeness.

The FCC report \cite{measured} also includes some data on latency, but only
measures latency from the consumer to the nearest measurement server. The
latencies measured here match up reasonably well with Doctom-Monitor's data,
maybe adding up to 50ms on each customer's side, with a maximum of 100ms for
customer-to-customer p2p connections.

\subsubsection{Asymmetry}
Broadband connections are rarely symmetrical in their up- and downstream
capabilities. Most connections by far provide much higher downstream
capabilities than upstream, limiting the available bandwidth for peer-to-peer
streaming at the upstream point.

While we may expect changes to this \cite{docsis31}, adoption of such new
technology takes time to roll out. For the time being, it's reasonable to
assume that upstream capabilities lie at 10-25\% of downstream capabilities, as
provided by current technologies.

\subsubsection{Peering}
Neither bandwidth estimate reflects issues of interconnectivity. In previous
work at
Joost, it was found that in China (serving as an example only), there were
very limited peering arrangements between ISPs. Decent bandwidth and latency
could routinely only be achieved between
customers of the same ISP, while connections between customers of different ISPs
were throttled so much as to be useless.

The principle of peering, i.e. offering a quid-pro-quo exchange of bandwidth
between ISPs has been the founding stone of Internet architecture since its
inception. China a decade ago demonstrates that this principle is not universally
applied, leading to the expectation that there will be pockets of the internet
effectively isolated from each other for peer-to-peer communication -- even if
bandwidth is available in principle.

\subsubsection{IP Network Constraints}
For the purposes of modern internet usage, it's safe to assume that traffic is
transported over IP at the network layer (assuming an IP compatible adjustment
to the OSI model).

\paragraph{Path MTU}
IPv4 and IPv6 place different constraints on packet sizes, but both are
eventually bounded by the underlying data link layer. In data centers, the data
link layer might be based on any protocol, but for consumer connections, it's
safe to assume that one of Ethernet or 802.11 are involved, and maybe a PPP
protocol layered on top of either, such as PPPoE for broadband connections.

That means path MTU for peer-to-peer connections is almost certainly in the
range of 1400-1500 Bytes \cite{wikimtu}. We'll assume the lower of these to be
on the safe side. The implication is that all peer-to-peer IP traffic will be
segmented into transmission units of ca. 1400 Bytes.

\paragraph{Packet Throughput Rate}
One of the characteristics of internet traffic typically disregarded in
transmission rates is that IP is a packet switching network. Specifically this
means that

\begin{enumerate}

  \item Each packet is transmitted in full over the underlying media before the
    next packet is processed, and

  \item routers or other processing units along the path will only process and
    therefore forward complete packets. Their capacity for processing packets
    is bounded by their hardware design.

\end{enumerate}

The implication is that the underlying hardware reason for bandwidth and
latency characteristics is the maximum packet throughput rate of the networking
equipment and links.

For applications streaming at high bandwidth, it is then imperative to keep the
packet throughput rate below the path capacity in order to achieve the
minimum possible latency -- higher throughput rates will inevitably fill
network interface buffers and may cause packet loss.

\paragraph{TCP vs. UDP} TCP is the default stream oriented protocol on top of
IP, and UDP the default datagram protocol. Both add a concept of ports to IP
connections, in order to multiplex different senders and recipients at the same
IP endpoint.

Their difference is that UDP does little more than that, whereas TCP adds a
delivery guarantee for each packet. The method chosen to do so is to number
bytes transmitted via a TCP connection, and for each received packet send
an acknowledgement of the last received bytes.

In terms of packet throughput rate, TCP therefore halves it for the same
payload bandwidth compared to UDP in order to guarantee delivery.

It's worth noting that the TCP SACK mechanism does not reduce the number of
acknowledgements sent -- it merely allows for more selective acknowledgements
and retransmissions in case a packet was dropped, but subsequent packets were
received. Without the SACK mechanism, only data up to the dropped packet could
be acknowledged.

\subsubsection{Casting}
The terminology section refers to {\em casting} as opposed to {\em streaming}.
It should be pointed out that several casting strategies are available at the
IP layer, but support for them is limited.

\begin{description}

  \item[Unicasting] is the default mode of transferring IP packets.

  \item[Narrowcasting] exists in the form of IP multicasting. Unfortunately, IP
    multicasting is in practice largely limited to a single IP network, or a group
    of IP networks under the management of the same organization. IP
    multicasting rarely works on the wider Internet.

  \item[Broadcasting] exists in the form of the broadcast address of each
    IP network, and is therefore limited to the network, and not routed
    outside.

  \item[Swarmcasting] does not exist on the IP layer.

\end{description}

The upshot is that any forms of casting other than {\em unicasting} must be
emulated at the application protocol level. IP-based multicasting or
broadcasting could be employed as optimizations for local networks.

Note that this is a viable strategy for e.g. IPTV services within hotels, etc.
An implementation of a media streaming system should at least not obstruct such
uses.

