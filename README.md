# Peer-to-Peer Media Storage and Casting

![The latest version of the document](p2p_storage_casting.pdf) is always
committed and linked here.

The document outlines challenges and approaches to high quality media
casting over a peer-to-peer network, given today's internet topology.
