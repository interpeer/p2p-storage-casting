# Contributing

We love merge requests from everyone. By participating in this project, you
agree to abide by the following documents:

* [Code of Conduct](https://codeberg.org/interpeer/p2p-storage-casting/src/branch/main/CODE_OF_CONDUCT.md)
* [License](https://codeberg.org/interpeer/p2p-storage-casting/src/branch/main/LICENSE.txt)
* [Developer Certificate of Origin](https://codeberg.org/interpeer/p2p-storage-casting/src/branch/main/DCO.txt)
  as applied to the license above.

## Fork Me!

Fork, then clone the repo:

    git clone git@codeberg.org:your-username/p2p-storage-casting.git

## Setup

**Requirements:**

* [XeTeX](http://xetex.sourceforge.net/)

Make sure to install XeLaTeX as well.

## Building

```bash
$ make
```
