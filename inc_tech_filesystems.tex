\subsection{Filesystems}
An implementation of a distributed media storage system shares some
similarities with local file system implementations. The main difference is
that in a distributed system, a consensus algorithm needs to be employed to
keep the system state valid.

A secondary difference is that storage and retrieval latencies are much higher
in a distributed system -- or potentially so. But the mitigation techniques of
more localized caching are identical (in the abstract).

Therefore, a simplified overview of file system architecture can inform the
design of a distributed storage system.

Some of the terminology used in this section is specific to UNIX operating
systems, but the concepts are transferrable.

At its most basic, a file system manages which data {\em blocks} of a block device
belong to a file. Block devices provide access to fixed-sized blocks of bytes,
so files may use zero to many blocks, and leave blocks partially filled. All
this is managed in what we'll conceptually call the {\em block allocation
layer}.

A typical optimization of block allocation is to store files as much as
feasible in sequential blocks. Such a block range is typically called an {\em
extent}.

In file sharing parlance, the term {\em chunk} is more often encountered. A
chunk is the more abstract term, encompassing single blocks as well as extents;
it is the basic unit of file subdivisions.

Block allocation information is stored in {\em inodes} (possibly derived from
"index node"), which also contain limited metadata about the file. The most
common form of metadata is the file name, a permission bitmap, etc. We'll call
this the conceptual {\em metadata layer} (even if in implementations it is
indistinguishable from the block allocation layer). The metadata layer at
minimum identifies a file.

On top of this, inodes also contain indexing information. Directory inodes are
file system entries just like file inodes, but instead of referencing allocated
blocks, they reference files contained in the directory. We'll call this the
{\em indexing layer}, and it's used for content discovery.

Another component of many file systems is a {\em journal}, in which the intent
of modifications is recorded before the modification is made, so that crash
recovery can be optimized. Journals effectively form a consensus algorithm
between the state before and after a crash.

An alternative to journals are full {\em copy-on-write} file systems which
write new data to newly allocated blocks, then update metadata to account for
the change, iterating all the way to updating the the file system root. If
journals can be compared to a consensus algorithm, copy-on-write can be compared
to eventual consistency.
