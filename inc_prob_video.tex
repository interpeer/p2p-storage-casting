\subsection{Video and Audio}
Video and audio streams have particular qualities over other types of files
that require recognition when designing streaming protocols.

\subsubsection{Bitrate}
Video streams encode more data than audio streams, and therefore require a
higher bitrate, so we can neglect audio stream bitrate requirements for the
moment.

Video stream bitrates depend highly on how noisy the image is, what quality
expectations you have, and which video codec you use. If we assume
1080p/30fps as the standard of today, Blu-Ray requirements would be up to
40Mbit/s using the H.264 codec.

You can always compromise on quality, and that is the typical strategy for
broadcasting over various media. Even so, typical bitrates are often no lower
than 12-13Mbit/s with H.264 for {\em "unnoticeable"} quality degradation.

Google’s VP9 has been shown to achieve up to 30\% better compression rates,
H.265 up to 50\%; and AV1 claims to outdo them both.

This leaves a fairly wide range of 12-40Mbit/s for H.264, with hopes that
H.265 or AV1 may lower them to 6-20Mbit/s. Given both ranges, it’s safe to
assume that in the 12-20Mbit/s range, good quality can be achieved. Video
streaming services often lower this to 5-10Mbit/s with reduced quality streams.

Of course, 1080p is strictly speaking yesterday’s standard -- we’re now living
in a world of increasing 4k videos, and 8k is starting to become the new high
end reference. Both formats quadruple bitrate requirements on resolution alone,
and routinely offer 60fps framerates instead of 1080p’s 30. With no further
reduction in quality or improvements in video codecs, we can assume the
following table provides reasonable data:

\begin{table}[h!]
\begin{tabular}{rrr}
                     & \multicolumn{1}{c}{\textbf{Good}} & \multicolumn{1}{c}{\textbf{Reduced}} \\
\textbf{1080p/30fps} & 12-20                               & 5-10                               \\
\textbf{4k/60fps}    & 96-160                              & 40-80                              \\
\textbf{8k/60fps}    & 384-640                             & 160-320
\end{tabular}
\caption{Bitrates in Mbit/s for different quality requirements and
  video formats.}
\end{table}

It becomes very apparent that at while at 1080p it may be possible to stream
video with fairly good quality, that becomes impossible for most internet
connections for 4k. Reduced quality may work for 4k, but streaming 8k is
basically reserved for LAN environments at the moment.

Video streaming providers spend a lot of effort on reducing these bitrate
requirements further; there surely is some leeway. But 8k streaming is outside
of most internet connection speeds by an order of magnitude, so compression
improvements will only do so much.

\subsubsection{Subjective Streaming Quality}
Research into subjective video quality during degraded conditions
\cite{perceivedquality, temporalquality} suggests that when other quality
factors such as the above cannot be reduced, frame loss conditions may have
less impact on perceived quality than could be expected.

Jitter, that is frame drops spread out over a period of time, is generally
perceived to be preferrable over jerkiness or buffering.

Within limits, then, a streaming service could introduce or tolerate jitter
better than congested paths between producer and consumer.

\subsubsection{Multiplexing}
Each video stream is effectively a multiplexed stream of video and audio data,
and may contain additional data streams. While downloadable videos do, in fact,
multiplex this data into single streams, storage media such as DVDs or Blu-Ray
do not.

What in storage media is offered as a convenience for bundling multiple audio
languages with the same video content, also becomes a strategy for keeping
transmission bitrate low in streaming.

A media streaming system should consider sourcing separate streams, and
multiplexing them for transmission and decoding. In order to do so, individual
bitrates in constant bitrate encodings, and additional bitrate changes in
variable bitrate encodings must be known by the streaming servers.
