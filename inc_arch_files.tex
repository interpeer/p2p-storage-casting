\subsection{File/Stream Abstraction}

PPSPP's organization of files into chunks, which are addressable via Merkle
trees is sound\footnote{It should be noted that sending hashes of data {\em
ahead} of data is susceptible to extension attacks, and the Merkle tree usage
in Dat and PPSPP does not mitigate against them. Double hashing is generally
considered a sufficient solution}, and it's no wonder that Dat has adopted it.
What is required on top of Dat's implementation, though, are the partial
updates to Merkle trees via munro hashes that PPSPP provides.

Some minor modifications of this scheme will allow for better privacy in the face
of forwarding peer-to-peer nodes that may not be fully trusted \footnote{In
peer-to-peer networks, it's easier to arrive at a sound design when your
default assumption is that no peer may be trusted.}.

\subsubsection{Encryption}

The obvious first modification is to encrypt the data chunks that make up the
file. In contrast to transport layer encryption method, blocks of data should
be randomly accessible, so the XTS-AES cipher \cite{xex-aes} is recommended.

The cipher does not contain any protection against modifications, such that any
random sequence of bytes will be "decrypted" to a different random sequence.
XTS-AES leaves it up to the application layer to protect against such issues.

The proposal is therefore to concatenate raw data chunks with a checksum
\footnote{A more complete proposal is to use a signing scheme here that,
combined with encryption, does not fall prey to the problems outlined in
Davis' analysis of common schemes \cite{defective-sign}.} before
encryption. The result of both operations forms the entry into the Merkle tree.
Such a checksum could conceivably be a cryptographically secure hash, as no
error recovery is necessarily intended. Note that such a hash would differ from
the hash entered into the Merkle tree.

Such a cipher requires the symmetric encryption key(s) to be communicated
out-of band, which naively implemented adds protocol complexity one would like
to avoid.

\subsubsection{Record Stream}

When splitting file data into a series of chunks, each chunk is appended to the
previous chunk to form the final file again. In casting situations, where there
is no concept of a full file until the stream ends, this is more apparent, but
applies to all chunking mechanisms.

The proposal is to make this explicit. Instead of adding file chunks to the
Merkle tree, add {\em append records} with file chunk data.

The overhead of this is very low -- naively a single byte header is
sufficient. But the benefit is that we can now multiplex other types of records
into the stream without need for out-of-band communications, such as symmetric
encryption keys.

Adding a header does carry security implications. For the most part, it
needs to be ensured that the header actually matches the following chunk data.
This is partially ensured by the chunk hashes in the Merkle tree.

\subsubsection{Key Exchange}

Key exchange can occur via X3DH \cite{x3dh}. Since that protocol is designed for
offline use, and we can now embed other records into our record stream, the proposal
is to embed some parts of the X3DH messages into the file stream.

In particular, X3DH requires Bob to publish prekeys to a server, which Alice
can use to establish a secure connection. Alice, then, wands to {\em send} data
securely, and Bob wants to {\em receive} data securely. The burden then falls
on file consumers to publish prekeys.

Prekey bundles contain public keys and signatures, nothing that cannot be
shared with the public. It is therefore conceivable to store them in a
distributed hash table used by the network (see following sections).

Once prekey bundles are successfully distributed on the network, the sender's
initial message to each accepted recipient can be embedded directly into the
Merkle tree. This initial message contains a shared key for further
communication with the recipient. In this proposal, the X3DH initial message
should immediately be followed by an encrypted message containing the file
encryption key.

\subsubsection{Extension Attack Mitigation}
\label{extension-mitigation}

As mentioned previously, the Merkle tree usage in Dat and PPSPP is susceptible
to extension attacks. The proposed full specification of each entry into the
tree that mitigates against this issue follows.

\begin{table}[h!]
\begin{bytefield}[bitwidth=1.1em]{16}
  \bitheader{0,7,8,15} \\
  \begin{rightwordgroup}{\#1}
    \wordbox{1}{Record Type} \\
    \wordbox{2}{Payload Size} \\
  \end{rightwordgroup} \\
  \begin{rightwordgroup}{\#2}
    \wordbox[tlr]{2}{Data Chunk\par$\cdots$} \\
    \skippedwords\\
    \wordbox[blr]{1}{} \\
    \wordbox{1}{Checksum} \\
  \end{rightwordgroup} \\
  \begin{rightwordgroup}{\#3}
    \wordbox{2}{Signature} \\
  \end{rightwordgroup} \\
\end{bytefield}
\end{table}

\begin{enumerate}
  \item We begin with a 16 bit field for the record type. Probably an 8 bit
      field would be sufficient, but let's add the extra extensibility.

  \item We follow with a payload size of 32 bit. This is the size of the
    encrypted data that follows. 32 bit would allow rather larger chunks than
    useful, but 16 bits would leave chunks artificially short\footnote{If
    extra overhead is a concern, an 8 bit record type and a 24 bit payload size
    would also align nicely into 32 bits total.}.

    This size contains the raw data chunk size, the checksum size, the size of
    any padding bytes added during encryption {\em and} the size of the final
    signature.

  \item The next part is the encrypted data chunk. It contains:
  \begin{enumerate}
    \item The raw data chunk.
    \item A checksum.
  \end{enumerate}

  \item Finally, a signature over the entire preceeding byte data is appended.

\end{enumerate}

The choice of checksum and signature algorithm is left up to the implementation.
It is conceivable for the signature to be a simple hash -- but in practice, the
best choice would be a public key signature as PPSPP proposes \cite{ppspp}.

Note that the entirety of \#2 above is encrypted, meaning the data chunk is
effectively verified with a sign-then-encrypt-then-sign scheme, where the
initial signature may only validate the data itself, i.e. with a checksum. This
scheme fixes common signing and encryption weaknesses, as well as extension
attacks -- though double hashing for any hash-based checksums is still
recommended.

Mismatching signatures or payload sizes must lead recipients to discard the
entire record. In fact, the use of signatures allow recipients to selectively
accept chunks from multiple signatories, as long as they have all been deemed
authoritative.

We define three types or records in this scheme so far:

\begin{enumerate}
  \item Append records that append file data,
  \item X3DH initial message records for specific recipients, and
  \item An encrypted file encryption key addressed to the same recipient.
\end{enumerate}

There is likely a good use-case for a fourth type of record, a metadata record.
The metadata payload is somewhat outside the scope of this section; however,
following sections may refer to such a record.

One metadata field that immediately springs to mind is a flag indicating
whether the file is encrypted at all. Public files do not need encryption and
the overhead associated with it. Nothing much changes in the rest of this
section, except that the payload size does not need to account for padding
bytes with unencrypted files, and only append records are necessary.
