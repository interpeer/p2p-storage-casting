%----------------------------------------------------------------------------------------
% METADATA
%----------------------------------------------------------------------------------------
\newcommand{\ptpAuthor}{Jens Finkhäuser}
\newcommand{\ptpAuthorEmail}{jens@finkhaeuser.de}
\newcommand{\ptpDocumentUrl}{https://codeberg.org/interpeer/p2p-streaming-architecture}

\newcommand{\ptpTitle}{Peer-to-peer Media Storage and Casting}
\newcommand{\ptpSubject}{Media storage and casting over peer-to-peer overlay
networks.}
\newcommand{\ptpKeywords}{video, audio, peer-to-peer, casting, streaming,
storage, distributed hash tables}
\newcommand{\ptpDate}{2020-00-00}
\newcommand{\ptpVersion}{Draft 4}

\newcommand{\ptpLicenseName}{Creative Commons Attribution-ShareAlike 4.0 International License}
\newcommand{\ptpLicenseAbbr}{CC BY-SA 4.0}
\newcommand{\ptpLicenseUrl}{https://creativecommons.org/licenses/by-sa/4.0/}

% NOTE Can e.g. improve this with https://tex.stackexchange.com/questions/94464/note-environment-with-mdframed
\newenvironment{note}{%
  \begin{quote}%
    {\bfseries Note:}%
}
{\end{quote}}
%----------------------------------------------------------------------------------------
% PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[twoside,twocolumn,a4paper]{article}

\usepackage{ifthen}

\usepackage[sc]{mathpazo} % Use the Palatino font
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\linespread{1.05} % Line spacing - Palatino needs more space between lines
\usepackage{microtype} % Slightly tweak font spacing for aesthetics

\usepackage[english]{babel} % Language hyphenation and typographical rules
\usepackage[english]{isodate}

\usepackage[hmarginratio=1:1,top=32mm,columnsep=20pt]{geometry} % Document margins
\usepackage[hang, small,labelfont=bf,up,textfont=it,up]{caption} % Custom captions under/above floats in tables or figures
\usepackage{booktabs} % Horizontal rules in tables

\usepackage{lettrine} % The lettrine is the first enlarged letter at the beginning of the text

\usepackage{enumitem} % Customized lists

\usepackage{bytefield} % For packet layouts

\usepackage{abstract} % Allows abstract customization
\renewcommand{\abstractnamefont}{\normalfont\bfseries} % Set the "Abstract" text to bold
\renewcommand{\abstracttextfont}{\normalfont\small\itshape} % Set the abstract itself to small italic text

\usepackage{titlesec} % Allows customization of titles
\renewcommand\thesection{\Roman{section}} % Roman numerals for the sections
\renewcommand\thesubsection{\roman{subsection}} % roman numerals for subsections
\titleformat{\section}[block]{\large\scshape\centering}{\thesection.}{1em}{} % Change the look of the section titles
\titleformat{\subsection}[block]{\large}{\thesubsection.}{1em}{} % Change the look of the section titles

\usepackage{fancyhdr} % Headers and footers
\pagestyle{fancy} % All pages have headers and footers
\fancyhead{} % Blank out the default header
\fancyfoot{} % Blank out the default footer
\fancyhead[C]{\ptpTitle\ -- \ptpVersion\ $\bullet$ {\printdayoff\printdate{\ptpDate}} $\bullet$
\href{\ptpLicenseUrl}{\ptpLicenseAbbr}} % Custom header text
\fancyfoot[RO,LE]{\thepage} % Custom footer text

\usepackage{titling} % Customizing the title section

\usepackage[
  backref=true,
  pagebackref=true,
  hyperindex=true,
  pdfborderstyle={/S/U/W 1},
  pdflang=EN,
  pdfpagelabels=true,
]{hyperref} % For hyperlinks in the PDF

\let\oldbackref\backref
\renewcommand*{\backref}[1]{[p.~\oldbackref{#1}]}

\usepackage{xcolor}

%----------------------------------------------------------------------------------------
% COMMANDS
%----------------------------------------------------------------------------------------

%----------------------------------------------------------------------------------------
% TITLE SECTION
%----------------------------------------------------------------------------------------

\setlength{\droptitle}{-4\baselineskip} % Move the title up

\pretitle{\begin{center}\Huge\bfseries} % Article title formatting
\posttitle{\end{center}} % Article title closing formatting
\title{\ptpTitle\ -- \ptpVersion} % Article title
\author{%
\ptpAuthor\ % Your name
<\href{mailto:\ptpAuthorEmail}{\ptpAuthorEmail}> % Your email address
%\and % Uncomment if 2 authors are required, duplicate these 4 lines if more
%\textsc{Jane Smith}\thanks{Corresponding author} \\[1ex] % Second author's name
%\normalsize University of Utah \\ % Second author's institution
%\normalsize \href{mailto:jane@smith.com}{jane@smith.com} % Second author's email address
}
\date{\printdate{\ptpDate}} % Leave empty to omit a date
\renewcommand{\maketitlehookd}{%

\begin{abstract}
\noindent
This document outlines the problem domain by detailing current characteristics of
video streams, internet connections and related technologies. It provides an
analysis of related work, and proposes an alternative solution.
\newline
This document is Copyright \textcopyright\ Jens Finkhäuser.\newline
This work is licensed under a
  \href{https://creativecommons.org/licenses/by-sa/4.0/}
  {Creative Commons Attribution-ShareAlike 4.0 International License}.
The document source is managed in \href{\ptpDocumentUrl}{this GitLab repository}.
\end{abstract}
}

\hypersetup{pdftitle={\ptpTitle\ -- \ptpVersion}}
\hypersetup{pdfsubject={\ptpSubject}}
\hypersetup{pdfauthor={\ptpAuthor}}
\hypersetup{pdfkeywords={\ptpKeywords}}


%----------------------------------------------------------------------------------------

\begin{document}
\maketitle

\section{Version History}

\begin{table}[h!]
\begin{tabular}{ll}
  \multicolumn{1}{c}{\textbf{Publication Date}} & \multicolumn{1}{c}{\textbf{Document Version}} \\
  \ptpDate                                      & \ptpVersion \\
  2020-01-08                                    & Draft 3 \\
  2019-06-06                                    & Draft 2 \\
  2019-01-11                                    & Draft 1 \\
\end{tabular}
\end{table}

\section{Terminology}

\input{inc_term.tex}

\section{Problem Domain}

This section outlines the problem domain of peer-to-peer media streaming by
analysing each domain component, and providing requirements derived from the
each.

\input{inc_prob_internet.tex}
\input{inc_prob_video.tex}
\input{inc_prob_privacy.tex}
\input{inc_prob_aaa.tex}

\section{Related Technologies}

This section outlines related technologies at a high level of abstraction, and
provides a short analysis of how the technology meets or fails to meet issues
of the problem domain.

\input{inc_tech_filesystems.tex}
\input{inc_tech_can.tex}
\input{inc_tech_ledbat.tex}
\input{inc_tech_http.tex}
\input{inc_tech_kademlia.tex}
\input{inc_tech_simhash.tex}
\input{inc_tech_doubleratchet.tex}
\input{inc_tech_merkle.tex}
\input{inc_tech_dat.tex}
\input{inc_tech_ipfs.tex}
\input{inc_tech_ppspp.tex}

\section{Proposed Architecture}

This section outlines a proposed architecture at a high level of abstraction.

\input{inc_arch_intro.tex}
\input{inc_arch_files.tex}
\input{inc_arch_aaa.tex}
\input{inc_arch_transport.tex}
\input{inc_arch_discovery.tex}
\input{inc_arch_misc.tex}

\input{inc_biblio.tex}
\end{document}

