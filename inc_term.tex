\subsection{Streaming vs. Casting}
The term {\em streaming} is nowadays synonymous
with consuming media online without having previously downloaded them.

It's worth recalling that this is not the only meaning.

\begin{itemize}

  \item In storage parlance, streaming just refers to sequential access of
    data as file systems or tape storage provide, as opposed to the
    {\em random access} mode granted by memory.

  \item In file format parlance, a streaming file format is one that
    structures data in records that can be sequentially consumed, in order
    to better support streaming storage access.

\end{itemize}

When used in the context of distributed storage, the term only disambiguates
against types of file synchronization that use non-sequential patterns, such
as employed by e.g. BitTorrent. The vast majority of file access methods are
in fact streaming methods, rendering the common usage somewhat inaccurate.

A competing term is {\em casting}, which in networking parlance exists in
different, well-defined variants:

\label{casting-terms}
\begin{description}

  \item[Unicasting] describes transferring a packet (or sequence of packets)
    from an originating node to a single recipient node.

  \item[Narrowcasting] ({\em multicasting} in IP) describes transferring such
    a packet from a single originating node to a defined set of recipient nodes.

  \item[Broadcasting] describes transferring such a packet to the world
    at large, for any interested recipient to pick up.

  \item[Swarmcasting] is sometimes used as a form of casting that originates
    at more than one node.

\end{description}

On the packet level, most media streaming solutions today are in fact
unicasting solutions. Many unicast streams might be served in parallel,
but each packet typically has a single recipient.

This document uses the {\em casting} terminology for disambiguation
when specific transport modes are implied, and {\em streaming} for
the case where transport modes are unspecified.

\subsection{Record Streams}

A useful definition for this document is that of {\em record streams}. As
described in the previous section, in file format parlance, a stream is any
sequence of records that can be consumed sequentially.

Video and audio files tend to come in streaming formats, defined as a sequence
of (conceptual) frames.

But other kinds of record streams exist -- conceptually, for example, a commit in a
git repository is a record. Since all commits after the root commit refer to
one or more preceding commits, a git repository can be laid out as sequential
records. In fact, temporally speaking, this is almost always the case -- except
when two commits are created at {\em exactly} the same time, based on {\em
exactly} the same preceding commit.

\subsubsection{SI vs IEC Units}

One of the problems plagueing computer science is the mixed use of SI units and
IEC units. In SI units, the prefix {\em mega-} denotes one million, whereas in
CS we more often use it to mean $2^{20}$.

Just like storage manufacturers, broadband providers tend to use SI units when
specifying Mbit/s. Operating systems typically use IEC units when specifying
file sizes in MiB.

This document keeps the use of SI unit Mbit/s for broadband uses, because it makes
no explicit reference to file sizes. Implementations must be aware, however,
that the {\em Mibit/s} requirements for streaming media files cannot be
fulfilled by {\em Mbit/s} broadband speeds of the same numeric value.

\subsubsection{Bandwidth}

The author(s) of this document understand that the {\em bandwidth} term is
not correctly applied to what should be described as data transfer or
transmission rates.

In common usage, though, {\em available bandwidth} is used synonymously
with a maximum possible data transfer rate; other usages of the word tend to be
similar, such as {\em required bandwidth} or {\em bandwidth usage}, etc.

For simplicity, this document conforms to the common usage.
