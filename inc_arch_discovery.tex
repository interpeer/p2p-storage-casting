\subsection{Content Discovery}

When discussing content discovery, it bears recalling the conceptual
distinction of a {\em metadata layer} and an {\em indexing layer} in file
systems. Recall that the former stores file metadata, and the latter
effectively stores file locations (within directories).

\subsubsection{Metadata Layer}

The metadata records alluded to in the file representation section cover most
of the immediate requirements on a metadata layer. That is, metadata records
should exist for and within each file that, at minimum, mark the file as
encrypted or unencrypted.

It is largely unimportant to the file representation in what form metadata
records are stored. All we can say for certain is that they should contain
structured data. The choice of serialization format is outside the scope of this
document.

\begin{note}
  The main consideration really is the generation of signatures for the records.
Popular representations such as YAML or JSON all suffer from the problem that
they need to be normalized in order to be compared -- multiple different byte
sequences can in fact refer to the same data. This can make validation of
signatures difficult as well, if a signature of generated content is to be
compared to previously serialized content, for example. See, for example, how
JWT \cite{jwt} solves this by signing a specific serialized form of the content.
\end{note}

Given some form of structured data, we define the following metadata
entries with special meaning. An unencrypted metadata record with (some of)
these fields MUST be the first record in a compliant record stream.

\begin{description}
  \item[\texttt{name}] \hfill\\
    Contains the name under which this file can be resolved. That is, resolving
    this name and retrieving the first record must result in a record identical
    to the one containing this field (but may come from a different peer).
    If the field occurs in subsequent metadata records, it MUST be ignored.

  \item[\texttt{encrypted}] \hfill\\
    A boolean flag indicating whether encryption is enabled for the file. If
    the flag is true, it means any chunk append records cannot be processed by
    higher application layers unless appropriate key sharing records have been
    received\footnote{As record streams are ordered, it is theoretically
    possible for subsequent metadata records to switch encryption of following
    blocks on or off again. For the sake of simplicity, the proposal is to only
    consider the flag in the first occurring metadata record.}.
\end{description}

Other metadata fields might help the application layer decide how to process
the file. The proposed optional fields are:

\begin{description}
  \item[\texttt{type}] \hfill\\
    An IANA Media Type \cite{iana-types}. This information helps the
    application layer decide early what sort of stream to process.

  \item[\texttt{bitrate}] \hfill\\
    An expected bitrate for the stream. This bitrate cannot sensibly specify the
    {\em actual} stream bitrate, if non-file chunks are multiplexed into the stream.
    For variable bitrate encoded data, this bitrate should represent a
    reasonable expected average. The field largely serves to inform the
    transport layer's congestion management algorithm.

    The bitrate and path MTU result in an \emph{target packet throughput rate},
    which is the more useful metric for the transport mechanism.

    Publishing the bitrate is very useful for sender and recipient, and should
    be the default:

    \begin{enumerate}
      \item Senders can better multiplex streams when they know the target
        packet throughput rate, prioritizing packets from each stream so that
        they meet the target.
      \item Recipients can detect when a sender fails to meet the target and
        find a new source, if available.
    \end{enumerate}

\end{description}

\subsubsection{Indexing Layer}

The indexing layer of the proposed system does not have to be particularly
complex. In particular, we do not need to transport file system hierarchy
information as e.g. Dat does.

However, we should let clients choose which substreams of a de-multiplexed
media stream to subscribe to, in order to maximize the efficient use of available
bandwidth.

The proposal is to publish an index file tying these different substreams
together. It is a regularly published file, and just as any other file must
start with a metadata record. However, the file does not need to include
anything other than metadata records, and in fact most likely shouldn't.

\begin{note}
  This is similar to how Dat handles indexing data. The difference is that Dat
  uses an indexing file format, whereas we can use metadata records.
\end{note}

Metadata records in this file must supply one additional field, but may
contain any number of application-defined fields. The additional field is:

\begin{description}
  \item[\texttt{contents}] \hfill\\
    The contents field is a list of reference entries to other streams.
  \item[\texttt{live}] \hfill\\
    The live field is a boolean flag indicating a live record stream, e.g. one
    containing a live video feed. It's an indication to the transport layer to
    switch to casting mode instead of on-demand mode, and applies to all
    referenced files alike. If absent, the default value is false.
\end{description}

Each reference entry defines the same mandatory and optional fields as metadata
fields described in the metadata layer. This allows for early selection of
substreams.

A requirement is, though, that the metadata entries listed here MUST match the
metadata entries in the resolved file, otherwise the reference is considered
invalid.

In addition to these fields, other fields are likely useful for selection by
the application layer, but are file type dependent. A video stream entry, for
example, should contain metadata specifying the stream resolution, for
selection by the user. Metadata on audio streams should include information on
the audio language. Specifying mandatory metadata for individual stream formats
is outside the scope of this document.

\begin{note}
  It is a fairly trivial exercise to extend this indexing layer to support file
  system hierarchies. For example, merely referencing another index file is
  enough to create a hierarchy, though this mechanism is not very efficient.

  If and when need for such hierarchies arises, this system is extensible
  enough to support them.
\end{note}

\subsubsection{Hash Table}

While the previous sections cover how metadata is structured and (minimal)
indices are managed, no mention is as yet made how file names are resolved.

First, it's important to note that in PPSPP, the so-called {\em swarm ID} is
used to identify a data stream. For casting, PPSPP requires signed Merkle
trees, which are considered optional for on-demand mode. If signed Merkle trees
are used, partial tree updates can be pushed to receivers, enabling
verification of new chunks without renegotating the Merkle tree root.

It is proposed to make this the default behaviour for all files. Under the
PPSPP specs, the swarm ID for signed Merkle trees is the public key used for
signing. In terms of our previously defined PKI, this is an ephemeral key --
that is, a key used solely for the signing of this particular stream.

With PPSPP's requirements thus covered, the proposal is to use a modification
of Kademlia for resolving file names.

\paragraph{Node IDs} or peer IDs in a network are the peer addresses of nodes
participating in the network. According to our previously defined PKI, those
are equivalent to device IDs.

Kademlia defines its key namespace to be 160 bits in length; the proposal is to
loosen the requirement somewhat and define the key namespace to be the same
length as device IDs. It's not unlikely that a hash over public keys that's
160 bits in size is the best choice; however, for future extensibility it may
be better to make that size based on other requirements of peer/node/device
IDs.

Since Kademlia expects node IDs to be hashes of this length, it further
requires value keys to also be hashes of this length, and then computes an XOR
distance as part of its algorithm.

The choice of cryptographic hashes for node IDs is a good one -- it allows for
a wide spread of IDs across the key namespaces, with the result that Kademlia's
hash table should be resilient to partial network failures.

\paragraph{Value Keys}

However, forcing a cryptographic hash function for value keys also creates a
wide spread across the namespace, meaning that related keys will not be
clustered together. In terms of resolving substreams faster, this is not a
desirable property.

The proposal, then, is to use SimHash hashing for value keys, and modify
Kademlia to use SimHash distances instead of XOR distances. Recall that SimHash
has the property that similar keys produce SimHash values that have similar
distances to (most) randomly chosen reference points, such as node IDs. This
property means that there is a high likelihood that a node serving one key will
also serve similar keys, leading to far fewer lookups.

The worry that this might lead to chokepoints on the network should be
mitigated by the modified LEDBAT scheme with feedback -- peers can easily
choose further distant nodes for substreams.

\paragraph{Hierarchical Naming}

One side-effect of locating names with common prefixes at the same nodes is
that this lends itself to hierarchical naming schemes.

We propose a mixture of URL and the reverse domain name scheme commonly used
in software namespacing: \texttt{de.finkhaeuser/p2p/storage\_casting.pdf}.

This scheme lends itself to defining namespace authorities just as in the
case of the Domain Name System (DNS). Within each namespaces, authorities
are free to organize related files into the same prefix.

Note that a system for managing ownership over a namespace is outside of the
scope of this document. However, claims such provided by JWT \cite{jwt},
signed by an appropriate authority would be verifiable -- the claim would
need to include a signing public key that can be used to verify all resources
within the namespace. In that, a system very similar to DNSSEC \cite{dnssec}
would provide for delegation and key update mechanisms.

\paragraph{k-bucket Sorting}

In unmodified Kademlia, nodes for each key distances are placed into a {\em
k-bucket}, which is sorted by uptime of the node. This mechanism implies that
stale nodes are rarely contacted, and drop out of the hash table entirely.

Unfortunately, uptime is not the only metric for choosing a desirable node. As
we have seen, latency increases dramatically with network topology distance --
it is clear that of two candidates, the one closer in network topology is far
more desirable than the other.

The proposal is to sort k-buckets not just by uptime, but also by other metrics
such as network distance.

The optimal procedure for this still needs to be determined. However, as a
starting point, a weighted average of normalized network distance and uptime
can be used. It should be noted that in the in Joost's streaming layer, using
network topology distance as a selection metric made an enormous difference for
reducing stutter, both due to reduced overall latency, but also because with
shorter paths, packet drops become far less likely.

Network distance, of course, can only be determined at the node -- that is, a
participant serving values in the Kademlia table can only measure its own
network distance (easily) in order to sort values in the k-bucket. However,
each querying node also maintains its own k-buckets, thereby cascading the
effect.

\paragraph{Hash Table Values}

In unmodified Kademlia, the protocol either returns values or it returns nodes
to query that are closer to the value. The latter is exactly what we want, and
should not be modified.

Naively in our case, the former should resolve SimHashes over file names to the
actual file -- but in the case of PPSPP, we instead need a swarm ID.

It should be the case that the node responding with the swarm ID can also serve
the file via PPSPP.

These optimizations over Kademlia therefore come to mind:

\begin{enumerate}
  \item in addition to key lookup, allow one-step subscription to the key. This
    is a useful property for subscribing to substreams containing actual
    content, and removes one round trip to the same node.

  \item if a subscription was requested, a node can respond with the swarm ID,
    but immediately also send the first metadata chunk of the subscribed file
    -- or whatever chunk is considered current in casting.

  \item in addition to the swarm ID, a response should always include relevant
    alternate peers for the same file. This allows the client to update its own
    k-buckets for faster switching to alternative sources.
\end{enumerate}
