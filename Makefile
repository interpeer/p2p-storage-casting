BASE = p2p_storage_casting
INPUT = $(BASE).tex
OUTPUT = $(BASE).pdf
TEMPFILES = \
	$(OUTPUT:.pdf=.log) \
	$(OUTPUT:.pdf=.out) \
	$(OUTPUT:.pdf=.brf) \
	$(OUTPUT:.pdf=.aux)

build: $(OUTPUT)

clean:
	rm -f $(TEMPFILES)

realclean: clean
	rm -f $(OUTPUT)

$(OUTPUT): $(INPUT) inc_*.tex Makefile
	@echo "Generating PDF..."
	@xelatex $(BASE) && xelatex $(BASE) >/dev/null
	@ls -lh $(OUTPUT)
